import numpy as np
from PIL import Image

BLACK = 0
WHITE = 255
BLACKLEVELLOW = 1180
BLACKLEVELHIGH = 1200
WHITELEVELLOW = 2200
COLUMNS = 80
VSYNCVALUE = 1100
SCALE = 4

with open("sampledatafulleframe.txt") as f:
    data = f.read()

def mapVal(valueIn, baseMin, baseMax, limitMin, limitMax):
	if(valueIn > baseMax):
		return limitMax
	elif(valueIn < baseMin):
		return limitMin
	return (((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin)


data = data.split('\n')
values = [];

array = np.zeros((500,COLUMNS*SCALE), np.uint8)
hcount = 0
vcount = 0

for row in data:
    values.append(int(row))

startOfFrameMaybe = False
startOfFrameYes = False
endOfFrame = False
startOfFrameCount = 0
endOfFrameCount = 0
lastValue = 0
value = 0
i = 0

while(i < len(data)):
	value = int(data[i])
	i += 1
	# Check for Vsync
	if (value <= BLACKLEVELLOW):
		startOfFrameCount += 1
		if (startOfFrameCount > 100):
			if (startOfFrameYes == False):
				startOfFrameMaybe = True
			else:
				endOfFrame = True
				startOfFrameYes = False
	# Check if frame should start
	elif (value > BLACKLEVELLOW and value < BLACKLEVELHIGH and startOfFrameMaybe == True):
		startOfFrameCount = 0
		startOfFrameYes = True
		print('Start of frame')
		break
	# Reset count otherwise
	else:
		startOfFrameCount = 0

while (i < len(values)):
	value = values[i]
	if (value < VSYNCVALUE):
		endOfFrameCount += 1
		if (endOfFrameCount > 50):
			print('End of frame')
			break
	else:
		endOfFrameCount = 0
	if (value < BLACKLEVELLOW and lastValue > BLACKLEVELLOW):
		vcount = 0
		hcount += 1
		lastValue = value
	elif (value > BLACKLEVELLOW):	
		if (vcount < COLUMNS):
			for diff in range(SCALE):
				array[hcount][SCALE*vcount+diff] = mapVal(value, BLACKLEVELLOW, 3000, BLACK, WHITE)
			vcount += 1
			lastValue = value

	i += 1

im = Image.fromarray(array)
im.save("result - stretched.png")
img = Image.open('result.png')
img.show()