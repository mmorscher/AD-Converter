#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

with open("sampledatafulleframe.txt") as f:
    data = f.read()

data = data.split('\n')
values = []
for row in data:
    values.append(int(row))
plt.plot(values)

plt.show()